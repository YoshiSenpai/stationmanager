import java.lang.StringBuilder;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.time.*;


public class Gui implements ActionListener {

  private static LocalTime sysTime;

  private JFrame frame, statsFrame;
  private ImageIcon iss;
  private JLabel img;
  private JMenuBar menuBar;
  private JMenu menu;
  private JMenuItem stats;
  private JButton crewStats, test, test2;
  private JPanel mainBottomPanel, mainRightPanel;
  private JTextArea textTime, textCrew;
  private JLabel time;
  private JScrollPane scrollTime;

  public Gui() {
    this.frame = new JFrame("Station Manager");
    this.statsFrame = new JFrame("Crew");
    this.iss = new ImageIcon("images/ISS.jpg");
    this.img = new JLabel(this.iss);
    this.menuBar = new JMenuBar();
    this.menu = new JMenu("Menu");
    this.stats = new JMenuItem("Statistics");
    this.crewStats = new JButton("Crew Statistics");
    this.mainBottomPanel = new JPanel(new FlowLayout());
    this.mainRightPanel = new JPanel();
    this.textTime = new JTextArea(1,1);
    this.time = new JLabel("Time:");
    this.scrollTime = new JScrollPane(textTime);
    this.test = new JButton("TEST");
    this.test2 = new JButton("TEST2");
    this.textCrew = new JTextArea();
  }

  public void drawGui() {
    this.addActionListeners();
    this.createMainFrame();
    this.createStatisticsFrame();
  }

  public void createMainFrame() {
    this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    this.textTime.setEditable(false);
    this.frame.getContentPane().add(this.img, BorderLayout.CENTER);
    this.frame.getContentPane().add(this.mainBottomPanel, BorderLayout.PAGE_END);
    this.frame.getContentPane().add(this.mainRightPanel, BorderLayout.LINE_END);
    this.mainRightPanel.setLayout(new BoxLayout(this.mainRightPanel, BoxLayout.PAGE_AXIS));
    this.crewStats.setAlignmentX((float)0.5);
    this.test.setAlignmentX((float)0.5);
    this.test2.setAlignmentX((float)0.5);
    this.mainRightPanel.add(this.crewStats);
    this.mainRightPanel.add(this.test);
    this.mainRightPanel.add(this.test2);
    this.mainBottomPanel.add(this.time);
    this.mainBottomPanel.add(this.textTime);
    this.textTime.setLineWrap(true);
    this.frame.setJMenuBar(this.menuBar);
    this.menuBar.add(this.menu);
    this.menu.add(this.stats);
    this.frame.pack();
    this.frame.setLocationRelativeTo(null);
    this.frame.setVisible(true);
  }

  public void createStatisticsFrame() {
    //  this.statsFrame.getContentPane().add(this.crewStats);
    this.statsFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    this.statsFrame.add(this.textCrew);
    this.textCrew.setEditable(false);
    this.statsFrame.setLocationRelativeTo(null);
    this.statsFrame.setSize(240, 240);

  }


  public void printCrew(Astronaut[] crew) {

    try {

      for (Astronaut a : crew) {

        textCrew.append(a.getName() + ", " + a.getAge() + ", " + a.getRole() + "\n");
      }
    } catch (Exception e) {

      return;
    }
  }

  public void printTime() {
    String sysClock = sysTime.now().toString();
    StringBuilder sb = new StringBuilder(sysClock);
    sb.delete(7, 11);
    sb.setLength(8);
    textTime.insert(sb.toString(), 0);
    ActionListener printer = new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        sb.replace(0, 8, sysTime.now().toString());
        sb.setLength(8);
        textTime.replaceRange(sb.toString(), 0, 8);
      }
    };
    new Timer(1000, printer).start();
  }

  public void addActionListeners() {
    this.crewStats.addActionListener(this);
    this.stats.addActionListener(this);
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    if (e.getSource() == stats) {
      this.statsFrame.setVisible(true);
    }

    if (e.getSource() == crewStats) {
      this.statsFrame.setVisible(true);
    }
  }
}
