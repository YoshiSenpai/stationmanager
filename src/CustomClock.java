public class CustomClock {

  private int hours;
  private int minutes;
//  private int seconds;

  public CustomClock(int h, int m) {
    this.hours = h;
    this.minutes = m;
//    this.seconds = s;
  }

  public CustomClock() {
    this.hours = 0;
    this.minutes = 0;
//    this.seconds = 0;
  }

  public void runCustomClock(TimeWarp warp, Gui gui) {

    while (true) {

      if (this.minutes < 59) {
        this.minutes++;
      } else if (this.hours < 23) {
        this.hours++;
        this.minutes = 0;
      } else {
        this.hours = 0;
      }

      try {
        Thread.sleep(100 / warp.getWarpSpeed());
      } catch (InterruptedException e) {
        Thread.currentThread().interrupt();
      }
    }
  }

  public int getHours() {
    return this.hours;
  }

  public int minutes() {
    return this.minutes;
  }
}
