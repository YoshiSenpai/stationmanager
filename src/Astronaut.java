public class Astronaut {

  private int age;
  private int skill;
  private String role;
  private String name;
  private String nationality;
  private String gender;
  private double happiness, sanity, health;

  public Astronaut(int age, int skill, String role, String name, String nationality, String gender) {
    this.age = age;
    this.skill = skill;
    this.role = role;
    this.name = name;
    this.nationality = nationality;
    this.gender = gender;
    this.happiness = 75.0;
    this.sanity = 75.0;
    this.health = 75.0;
  }

  public String getName() {
    return this.name;
  }

  public String getRole() {
    return this.role;
  }

  public String getNationality() {
    return this.nationality;
  }

  public String getGender() {
    return this.gender;
  }

  public int getAge() {
    return this.age;
  }

  public int getSkill() {
    return this.skill;
  }

  public double getHappiness() {
    return this.happiness;
  }

  public double getSanity() {
    return this.sanity;
  }

  public double getHealth() {
    return this.health;
  }

  public void setHappiness(double newHappiness) {
    this.happiness = newHappiness;
  }

  public void setSanity(double newSanity) {
    this.sanity = newSanity;
  }

  public void setHealth(double newHealth) {
    this.health = newHealth;
  }
}
