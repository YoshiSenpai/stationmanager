public class Agency {

  private String name;
  private double funds;
  private int techLevel;

  public Agency(String name, double funds) {
    this.name = name;
    this.funds = funds;
    this.techLevel = 1;
  }

  public int getTechLevel() {
    return this.techLevel;
  }
}
