public class Event {

  private double chance;
  private String name, eventText;
  private boolean isBad;


  public Event(double chance, String name, boolean isBad) {

    this.chance = chance;
    this.name = name;
    this.isBad = isBad;
  }


  public void setEventText(String text) {

    this.eventText = text;
  }


  public String getName() {

    return this.name;
  }


  public String getEventText() {

    return this.eventText;
  }


  public boolean isEventBad() {

    return this.isBad;
  }


  public double getChance() {

    return this.chance;
  }
}
