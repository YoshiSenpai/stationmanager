public class TimeWarp {

  private int warpSpeed;
  private int originalWarpSpeed;
  private boolean warpSpeedChanged;

  public TimeWarp() {
    this.warpSpeed = 1;
    this.warpSpeedChanged = false;
    this.originalWarpSpeed = this.warpSpeed;
  }

  public void setWarpSpeed(int speed) {
    this.warpSpeed = speed;

    if (this.warpSpeed != this.originalWarpSpeed) {
      this.warpSpeedChanged = true;
      this.originalWarpSpeed = this.warpSpeed;
    } else {
      this.warpSpeedChanged = false;
    }
  }

  public int getWarpSpeed() {
    return this.warpSpeed;
  }

  public boolean checkWarpSpeedChange() {
    return this.warpSpeedChanged;
  }
}
