import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.time.*;

public class Main {

  public static void main(String[] args) {
    Gui gui = new Gui();
    Agency nasa = new Agency("NASA", 100000.0);
    Station station = new Station(nasa, "ISS", 8, 10, 180 * 8, 2 * 8, 30 * 8);
    Astronaut mark = new Astronaut(32, 50, "Scientist", "Mark Hamill", "USA", "Male");
    Astronaut joonas = new Astronaut(19, 50, "Engineer", "Joonas Schildt", "FIN", "Male");

    station.addCrew(mark);
    station.addCrew(joonas);

    gui.drawGui();
    gui.printTime();
    gui.printCrew(station.getCrew());
  }

  public static void pause() {

  }

}
