import java.lang.Math;

public class Station {

  private int crewCapacity;
  private int cargoCapacity;
  private int foodCapacity;
  private int oxygenCapacity;
  private int waterCapacity;
  private int currentCrewCount;
  private int currentCargoWeight;
  private int currentFood;
  private int currentOxygen;
  private int currentWater;
  private int techLevel;
  private String name;
  private Agency agency;
  private Astronaut[] crew;

  public Station(Agency agency, String name, int crewCapacity, int cargoCapacity, int foodCapacity, int oxygenCapacity, int waterCapacity) {
    this.agency = agency;
    this.name = name;
    this.crewCapacity = crewCapacity;
    this.cargoCapacity = cargoCapacity;
    this.foodCapacity = foodCapacity;
    this.oxygenCapacity = oxygenCapacity;
    this.waterCapacity = waterCapacity;
    this.currentCrewCount = 0;
    this.currentCargoWeight = 0;
    this.currentWater = waterCapacity;
    this.currentFood = foodCapacity;
    this.currentOxygen = oxygenCapacity;
    this.techLevel = agency.getTechLevel();
    this.crew = new Astronaut[this.crewCapacity];
  }

  public void addCrew(Astronaut astronaut) {

    if(this.currentCrewCount < this.crewCapacity) {

      this.crew[this.currentCrewCount] = astronaut;

      this.currentCrewCount++;
    }
  }

  public Astronaut[] getCrew() {

  /*  if (this.currentCrewCount < this.crewCapacity) {

      return this.crew[i];
    } else {

    }
  */    return this.crew;
  }

  public int getCrewCount() {

    return this.currentCrewCount;
  }


}
